"""
  Deploy Amazon s3 EMR cluster
"""

import argparse
import logging
import os
import subprocess

LOG_LEVEL = logging.DEBUG
LOGFORMAT = "  %(log_color)s%(levelname)-8s%(reset)s | %(log_color)s%(message)s%(reset)s"
from colorlog import ColoredFormatter
logging.root.setLevel(LOG_LEVEL)
formatter = ColoredFormatter(LOGFORMAT)
stream = logging.StreamHandler()
stream.setLevel(LOG_LEVEL)
stream.setFormatter(formatter)
log = logging.getLogger('pythonConfig')
log.setLevel(LOG_LEVEL)
log.addHandler(stream)


def get_args():
    parser = argparse.ArgumentParser(description='TicketNetwork S3Bucket Creation')

    ''' 
    Add Arguments
    '''
    parser.add_argument('-d', '--developer', type=str, help='Developer Username i.e charles.adetiloye ', required=True)
    parser.add_argument('-p', '--project', type=str, help='Project i.e campaign-automation ', required=True, default='TicketNetwork App')
    parser.add_argument('-s', '--profile', type=str, help='AWS credentials in ~/.aws/credentials ', required=True)
    parser.add_argument('-l', '--release_label', type=str, help='AWS EMR release labels', required=False, default='emr-5.6.0')
    parser.add_argument('-a', '--applications', type=str, help='Applications that will be installed on the cluster ', required=False, default='Name=Spark Name=Zeppelin')
    parser.add_argument('-t', '--should_auto_terminate', type=str, help='Indicate if EMR should auto terminate or Not', required=False, default='true')

    args = parser.parse_args()

    dev = args.developer
    proj = args.project
    prf = args.profile
    lbl = args.release_label
    apps = args.applications
    auto_terminate = args.should_auto_terminate

    return dev, proj, prf, lbl, apps, auto_terminate


'''
 Match return values from get_arguments()
 and assign to their respective variables
'''
developer, project, profile, release_label, applications, auto_terminate  = get_args()

log.info( "\nDeveloper: [ %s ]" % developer)
log.info( "Project: [ %s ]" % project)
log.info( "Profile: [ %s ]" % profile)
log.info( "EMR Release Version: [ %s ]" % release_label)
log.info( "Apps On this cluster: [ %s ]\n" % applications)
log.info( "Auto Terminate EMR cluster: [%s]" % auto_terminate)

playbook_tmpl = '''---

- hosts: localhost
  connection: local
  gather_facts: no
  roles:
     -  role: ec2-emr-clusters
        developer_name: %s
        project_name: %s
        cluster_name: '{{ project_name }}-{{ developer_name }}'
        applications: %s
        release_label: %s
        s3_app_bucket: bip-dev-{{ developer_name }}
        instance_groups_url: s3://{{ s3_app_bucket }}/{{ project_name }}/conf/instance-groups.json
        bootstrap_action_url: s3://{{ s3_app_bucket }}/{{ project_name }}/conf/bootstrap-action.sh
        ec2_attributes_url: s3://{{ s3_app_bucket }}/{{ project_name }}/conf/ec2-attributes.json
        steps: s3://{{ s3_app_bucket }}/{{ project_name }}/conf/steps.json
        should_auto_terminate_cluster: %s
'''

playbook = playbook_tmpl % (developer, project, applications, release_label, auto_terminate )

log.info("Generating Playbook ...")
log.info(playbook)

playbook_filename = "playbook_emr_%s.yml" % developer
playbook_file = open(playbook_filename, 'w')
playbook_file.write(playbook)
playbook_file.close()

log.info("")
log.info("[%s] created!", playbook_filename)
log.info("")
log.info("Running Ansible Playbook to create EMR cluster")

ansible_command = "AWS_PROFILE='ticket-network' ansible-playbook  %s -i  hosts.ini  -vvvv" % playbook_filename
subprocess.call(ansible_command, shell=True)

os.remove(playbook_filename)


