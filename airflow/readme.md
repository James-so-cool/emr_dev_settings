
#setup

1. be sure to install boto3 -> sudo pip install boto3
2. be sure to install awscli -> sudo pip install awscli
3. do `aws configure` to install the aws credentials
4. ensure your dag is deployed `$AIRFLOW_HOME/dags`
5. run the following command to validate the dags ok and good to go

`airflow list_dags` -> shows your dag is present <br>
i.e `airflow_emr_spark_kafka_logstash_dag.py`

`airflow list_tasks airflow_emr_spark_kafka_logstash_dag` -> shows active tasks on your dag
i.e 
`add_steps` <br>
`create_job_flow` <br>
`remove_cluster` <br>
`watch_step`


Be sure to have your compiled JAR pushed up to S3 from kafka-logstash-processor
`sbt s3-upload`



