#!/bin/bash

sudo yum -y install git
wget http://repo.continuum.io/archive/Anaconda2-4.0.0-Linux-x86_64.sh
sudo mkdir /mnt/var/virtualenvs
sudo mv Anaconda2-4.0.0-Linux-x86_64.sh /mnt/var/virtualenvs
sudo bash /mnt/var/virtualenvs/Anaconda2-4.0.0-Linux-x86_64.sh -b -p /mnt/var/virtualenvs/anaconda2
sudo rm /mnt/var/virtualenvs/Anaconda2-4.0.0-Linux-x86_64.sh
sudo /mnt/var/virtualenvs/anaconda2/bin/pip install kafka-python==0.9.5 pyhocon pygeoip elasticsearch user_agents boto3 scikit-neuralnetwork httplib2 geopy googleads bingads
sudo /mnt/var/virtualenvs/anaconda2/bin/pip install git+git://github.com/verisign/python-confluent-schemaregistry
wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCity.dat.gz
gzip -d GeoLiteCity.dat.gz
sudo mv GeoLiteCity.dat /mnt/var/virtualenvs/GeoLiteCity.dat
sudo aws s3 cp s3://bip-dev-james.luan/jamesDev/accessories /home/hadoop --recursive
sudo /mnt/var/virtualenvs/anaconda2/bin/pip install geojson
sudo /mnt/var/virtualenvs/anaconda2/bin/pip install shapely 
sudo /mnt/var/virtualenvs/anaconda2/bin/pip install shape
sudo yum install -y geos-devel